FROM openjdk:8-jdk

ADD target/employee-on-boarding-1.0.0-SNAPSHOT.jar /employee-on-boarding.jar

CMD ["java","-jar","/employee-on-boarding.jar"]