package sigma.software.bpm.onboarding;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * This is an easy adapter implementation
 * illustrating how a Java Delegate can be used
 * from within a BPMN 2.0 Service Task.
 */
@Component
public class EmpowermentDelegate implements JavaDelegate {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmpowermentDelegate.class);

    public void execute(DelegateExecution delegateExecution) {
        LOGGER.info("Adding personal data and permissions for the employee: "
                + "firstName=" + delegateExecution.getVariable("firstName")
                + ", lastName=" + delegateExecution.getVariable("lastName")
                + ", position='" + delegateExecution.getVariable("position")
                + ", department=" + delegateExecution.getVariable("department")
                + ", assignedTrainings=" + delegateExecution.getVariable("assignedTrainings")
                + ", intermediateFeedback=" + delegateExecution.getVariable("intermediateFeedback")
                + ", finalFeedback=" + delegateExecution.getVariable("finalFeedback"));

        LOGGER.info("Executed EmpowermentDelegate - {}", delegateExecution);
    }
}

