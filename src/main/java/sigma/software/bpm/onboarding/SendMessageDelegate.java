package sigma.software.bpm.onboarding;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * This is an easy adapter implementation
 * illustrating how a Java Delegate can be used
 * from within a BPMN 2.0 Service Task.
 */
@Component
public class SendMessageDelegate implements JavaDelegate {

    private static final Logger LOGGER = LoggerFactory.getLogger(SendMessageDelegate.class);

    private static final String VARIABLE_NAME = "messageName";

    @Override
    public void execute(DelegateExecution execution) {
        String businessKey = execution.getProcessBusinessKey();
        String messageName = (String) execution.getVariable(VARIABLE_NAME);

        LOGGER.info("Sending message {} with business key {}", messageName, businessKey);

        execution.getProcessEngineServices()
                .getRuntimeService()
                .createMessageCorrelation(messageName)
                .processInstanceBusinessKey(businessKey)
                .correlate();
    }
}