package sigma.software.bpm.onboarding;

import org.camunda.bpm.spring.boot.starter.event.PostDeployEvent;
import org.camunda.bpm.spring.boot.starter.event.PreUndeployEvent;
import org.camunda.bpm.spring.boot.starter.event.ProcessApplicationEvent;
import org.camunda.bpm.spring.boot.starter.event.ProcessApplicationStartedEvent;
import org.camunda.bpm.spring.boot.starter.event.ProcessApplicationStoppedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class CamundaEventListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(CamundaEventListener.class);

    @EventListener
    public void onPostDeploy(PostDeployEvent event) {
        LOGGER.info("PostDeployEvent: {}", event);
    }

    @EventListener
    public void onPreUndeploy(PreUndeployEvent event) {
        LOGGER.info("PreUndeployEvent: {}", event);
    }

    @EventListener
    public void onProcessApplication(ProcessApplicationEvent event) {
        LOGGER.info("ProcessApplicationEvent: {}", event);
    }

    @EventListener
    public void onProcessApplicationStarted(ProcessApplicationStartedEvent event) {
        LOGGER.info("ProcessApplicationStartedEvent: {}", event);
    }

    @EventListener
    public void onProcessApplicationStopped(ProcessApplicationStoppedEvent event) {
        LOGGER.info("ProcessApplicationStoppedEvent: {}", event);
    }
}