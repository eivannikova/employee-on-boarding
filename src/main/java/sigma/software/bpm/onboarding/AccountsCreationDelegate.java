package sigma.software.bpm.onboarding;

import org.apache.commons.lang3.time.DateUtils;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This is an easy adapter implementation
 * illustrating how a Java Delegate can be used
 * from within a BPMN 2.0 Service Task.
 */
@Component
public class AccountsCreationDelegate implements JavaDelegate {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountsCreationDelegate.class);

    private static final String DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

    private static final String TRIAL_PERIOD_DURATION = "duration";

    private static final String INTERMEDIATE_PERIOD_DURATION = "durationIntermediate";

    public void execute(DelegateExecution delegateExecution) {
        LOGGER.info("Creating work accounts for the employee based on specified personal data: "
                + "firstName=" + delegateExecution.getVariable("firstName")
                + ", lastName=" + delegateExecution.getVariable("lastName")
                + ", position='" + delegateExecution.getVariable("position")
                + ", department=" + delegateExecution.getVariable("department")
                + ", dateOfBirth=" + delegateExecution.getVariable("dateOfBirth")
                + ", address=" + delegateExecution.getVariable("address"));

        setTimerDates(delegateExecution);

        LOGGER.info("Executed AccountsCreationDelegate - {}", delegateExecution);
    }

    private void setTimerDates(DelegateExecution delegateExecution) {
        DateFormat df = new SimpleDateFormat(DATE_PATTERN);
        Date date = new Date();
        delegateExecution.setVariable(TRIAL_PERIOD_DURATION, df.format(DateUtils.addMinutes(date, 4)));
        delegateExecution.setVariable(INTERMEDIATE_PERIOD_DURATION, df.format(DateUtils.addMinutes(date, 2)));
    }
}