package sigma.software.bpm.onboarding;

import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableProcessApplication("onBoardingApplication")
public class OnBoardingApplication {
    public static void main(String... args) {
        SpringApplication.run(OnBoardingApplication.class, args);
    }
}
