package sigma.software.bpm.onboarding;

import org.camunda.bpm.engine.spring.SpringProcessEngineServicesConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Configuration used to import configurations from camunda process engine services as beans.
 *
 * @see org.camunda.bpm.engine.spring.SpringProcessEngineServicesConfiguration
 */
@Configuration
@Import(SpringProcessEngineServicesConfiguration.class)
public class CamundaConfiguration {

}