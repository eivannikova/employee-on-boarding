# Employee on-boarding process

![](./src/main/resources/images/employee-onboarding.png)

Process runs in its own spring-boot application build with the [camunda-spring-boot-starter](https://docs.camunda.org/manual/7.8/user-guide/spring-boot-integration/).

There is an ability to send messages to engine via REST API. See the [REST API](https://docs.camunda.org/manual/7.8/reference/rest/message/post-message/) documentation for details how to send a message. The correlation is done with the business key.

This rest call can be easily implemented in the application using [Spring RestTemplate](https://spring.io/guides/gs/consuming-rest/). _RestTemplate_ comes with the dependency to spring-web and is easy to code in a [delegate](https://docs.camunda.org/manual/7.8/user-guide/process-engine/delegation-code/) class.

Process engine data is stored in PostgreSQL. See related [documentation](https://docs.camunda.org/manual/7.8/user-guide/process-engine/database/).

## Process description

**Introduction to the Company:** New employee gets acquainted with Company's standards, structure and policy. _HR Manager_ assists with it and fills in the form with employee personal data within the _user task_.

**Work accounts creation:** Accounts are created automatically in the [service task](./src/main/java/sigma/software/bpm/onboarding/AccountsCreationDelegate.java) for the new employee based on personal data filled by _HR Manager_. (Service tasks are implemented as "mock" ones).

**Working machine selection:** System assigns working machine to employee using the [business rule](./src/main/resources/working-machine-selection.dmn).

**Introduction to the team and responsibilities:** _Leader_ introduces new team member, explains work content and employee responsibilities. Upon completion of this leader closes _user task_.

Then employee starts the work within the trial period. Further flow is parallelized into _two_ branches, which represent interaction with _Mentor_ and _Leader_:

**Tasks assignment and execution control:** Employee obtains working machine and completes necessary environment setup (this action is emulated by sending message to process via REST API):

```
    curl -X POST http://localhost:8080/rest/message \
        -H "Content-Type: application/json" \
        -d '{"messageName" : "environmentSetupCompleted", "businessKey": "<process_business_key>"}'
```

As soon as this messages has been received _Leader_ starts interaction with new employee: assigns the tasks and control the execution (_manual task_).

**Assessment, education scheduling:** _Mentor_ performs employee assessment, provides related recommendations. Also _Mentor_ schedules additional trainings if needed and fills in recommendation and trainings information within the _user task_ completion.

Process contains _two timers_ which are triggered by a certain dates (1 and 3 months after the trial period has been started). It is achieved by initializing related variables in the [code](./src/main/java/sigma/software/bpm/onboarding/AccountsCreationDelegate.java#L45) which are also defined in the _Timer Definition_ on diagram.

**Intermediate adaptation feedback:** _Leader_ fills the feedback within this _user task_ when the first 1-month timer has been triggered. As soon as this action has been completed intermediate feedback message is sent within related [delegate](./src/main/java/sigma/software/bpm/onboarding/SendMessageDelegate.java).

**Recommendations based on adaptation feedback:** This step is waiting for the _intermediate feedback message_ sent by the [delegate](./src/main/java/sigma/software/bpm/onboarding/SendMessageDelegate.java). This step implies _Methors's_ recommendations to employee based on the _intermediate feedback_. 

**Final collaboration decision:** End of the trial period comes when 3-months timer has been triggered. After that _Leader_ provides _final collaboration decision_ based on the overall feedback. _Leader_ completes the form within this _user task_ by marking if the decision on further collaboration was positive or not.  

**Employee empowerment in the working system:** If the final collaboration decision was positive, system empowers the employee within the automated [service task](./src/main/java/sigma/software/bpm/onboarding/EmpowermentDelegate.java). In this case collaboration is continued. Otherwise employee obtains refusal.

## How to run the example

Open terminal window from project directory containing this [docker-compose.yml](./src/main/resources/docker/docker-compose.yml) and run the command:

```
    docker-compose up    
```

Then build an application with the following command:

```
    mvn clean package
    java -jar ./target/employee-on-boarding-1.0.0-SNAPSHOT.jar
```

**Tomcat server** runs on port 8081.

**Adminer editor for PostgreSQL** runs on port 8082.

To inspect the _Cockpit_, open http://localhost:8081/app/cockpit/default/#/dashboard. Login with username _kermit_, password _superSecret_.

To start new process instance go to http://localhost:8081/app/tasklist/default/, click **Start process** button on the top of the window and select **Employee on-boarding** process. Enter the business key and monitor the process via the **Cockpit**.

To inspect _Adminer editor for PostgreSQL_, open http://localhost:8082. Select _PostgreSQL_ as system and login with username _admin_, password _admin_.

As the REST Api is deployed, you can reach the engines via rest client. For example, the url http://localhost:8081/rest/history/process-instance/count?finished=true can be used to get the number of completed process instances from the engine.

## Run the example on Docker

Open terminal window in the root project directory and build project with maven:

```
mvn clean package
```

Afterwards build the docker image:

```
docker build -t employee-on-boarding .
```

Start the docker composition:

```
docker-compose up
```

Inspect container logs:

```
docker-compose logs employee-on-boarding
```

The [docker-compose.yml](docker-compose.yml) file allows to start _Employee on-boarding application_, _PostgreSQL_ and _Adminer editor for PostgreSQL_ in docker containers within **on-boarding-network**. Application is accessible in the same way as described in previous section. 